//
//  AnimeDetailService.swift
//  HomeworkAnimeAPI
//
//  Created by Журавлев Лев on 01.12.2023.
//

import Foundation

class AnimeDetailService {
    
    private let animeParser = AnimeDetailXMLParser()
    private let mangaParser = MangaParser()
    
    func fetchAnimeDetails(
        type: TypesOfAnimeContent,
        for animeId: String,
        completion: @escaping (Result<AnimeDetailsModel, Error>) -> Void
    ) {
        let urlString = "https://cdn.animenewsnetwork.com/encyclopedia/api.xml?\(type.rawValue)=\(animeId)"
        
        if let url = URL(string: urlString) {
            let task = URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
                if let error = error {
                    completion(.failure(error))
                    return
                }

                guard let data = data else {
                    completion(.failure(NSError(domain: "NoData", code: 0, userInfo: nil)))
                    return
                }

                do {
                    let xmlString = String(data: data, encoding: .utf8)
                                        
                    self?.animeParser.parseXML(xmlString: xmlString ?? "")
                    if let animeDetails = self?.animeParser.animeDetails {
                        completion(.success(animeDetails))
                    } else {
                        completion(.failure(NSError(domain: "ParsingError", code: 0, userInfo: nil)))
                    }
                } catch {
                    completion(.failure(error))
                }
            }

            task.resume()
        }
    }
}

