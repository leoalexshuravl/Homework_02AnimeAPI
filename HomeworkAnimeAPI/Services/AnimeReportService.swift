//
//  AnimeReportService.swift
//  HomeworkAnimeAPI
//
//  Created by Журавлев Лев on 01.12.2023.
//

import Foundation

class AnimeReportService {
    
    private let parserDelegate = ReportXMLParserDelegate()

    func fetchCachedReport(completion: @escaping (Result<Report?, Error>) -> Void) {
        
        parserDelegate.getCachedReport{ report, error in
            completion(.success(report))
        }
    }
    
    func fetchReport(for animeType: TypesOfAnimeContent, completion: @escaping (Result<Report?, Error>) -> Void) {
        let urlString = "https://cdn.animenewsnetwork.com/encyclopedia/reports.xml?id=155&type=\(animeType)&nlist=50"
        
        guard let url = URL(string: urlString) else {
            completion(.failure(NSError(domain: "Invalid URL", code: 0, userInfo: nil)))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = data else {
                completion(.failure(NSError(domain: "No data received", code: 0, userInfo: nil)))
                return
            }
            
            let parsedReport = self?.parserDelegate.parse(data: data)

            completion(.success(parsedReport))
        }
        
        task.resume()
    }
}

