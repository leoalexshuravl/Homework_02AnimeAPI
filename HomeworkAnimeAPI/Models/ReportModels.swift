//
//  ReportModels.swift
//  HomeworkAnimeAPI
//
//  Created by Журавлев Лев on 01.12.2023.
//

import Foundation

struct AnimeListModel {
    var type: TypesOfAnimeContent
    var reportItems: [ReportItem]
}

struct ReportItem: Codable, Identifiable {
    var id: String
    var gid: String
    var type: String
    var name: String
    var precision: String
    var vintage: String?
}

struct Report: Codable {
    var skipped: String
    var listed: String
    var args: ReportArgs
    var items: [ReportItem]
}

struct ReportArgs: Codable {
    var type: String
    var name: String
    var search: String
}

enum TypesOfAnimeContent: String {
    case anime
    case manga
    case TV
    case ONA
    
    init?(rawValue: String) {
        switch rawValue {
        case TypesOfAnimeContent.anime.rawValue:
            self = .anime
        case TypesOfAnimeContent.manga.rawValue:
            self = .manga
        case TypesOfAnimeContent.TV.rawValue:
            self = .TV
        case TypesOfAnimeContent.ONA.rawValue:
            self = .ONA
        default:
            self = .anime
        }}
    
}
