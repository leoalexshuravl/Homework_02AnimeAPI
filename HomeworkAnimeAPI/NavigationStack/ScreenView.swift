//
//  ScreenView.swift
//  HomeworkAnimeAPI
//
//  Created by Журавлев Лев on 20.02.2024.
//

import SwiftUI

enum Transition {
    case none
    case custom(AnyTransition)
}

enum NavigationType {
    case push
    case pop
    case popToRoot
}

struct Screen: Identifiable, Equatable {
    
    let id: String = UUID.init().uuidString
    let screenView: AnyView
    
    static func == (lhs: Screen, rhs: Screen) -> Bool {
        lhs.id == rhs.id
    }    
}


protocol NavigationActions {
    func top() -> Screen?

    mutating func push(_ s: Screen)

    mutating func popToPrevious ()
    
    mutating func popToRoot()
}


struct ScreenStack: NavigationActions {
    
    var screens: [Screen] = []
    
    func top() -> Screen? {
        self.screens.last
    }

    mutating func push(_ s: Screen) { self.screens.append(s) }

    mutating func popToPrevious () { _ = self.screens.popLast() }
    
    mutating func popToRoot() { self.screens.removeAll() }
}

class NavigationViewModel: ObservableObject {
    static var shared = NavigationViewModel()
    var navigationType: NavigationType = .push
    
    @Published var currentScreen: Screen?

    private var screenStack = ScreenStack() {
        didSet { currentScreen = screenStack.top() }
    }

    func push(_ screenView: AnyView) {
        let screen = Screen(screenView: screenView)
        screenStack.push(screen)
    }
    
    func pop() { screenStack.popToPrevious() }

    func popToRoot() { screenStack.popToRoot() }
}

struct NavigationContainerView<Content>: View where Content: View & IItemView {
    @ObservedObject private var viewModel: NavigationViewModel = NavigationViewModel.shared
    
    private let content: NavigationItemContainer<Content>
    private let animation: Animation = .easeOut(duration: 0.33)
    private let transition: (push: AnyTransition, pop: AnyTransition)
    
    init(transition: Transition = .custom(.move(edge: .leading)),
         @ViewBuilder content: @escaping () -> Content
    ) {
        self.content = NavigationItemContainer(content: content())
        switch transition {
        case .custom(let transition):
            self.transition = (transition, transition)
        case .none:
            self.transition = (.identity, .identity)
        }
    }
    
    var body: some View {
        let isRoot = viewModel.currentScreen == nil
        return 
        VStack {
            if !isRoot {
                HStack {
                    Button {
                        viewModel.pop()
                    } label: {
                        Text("Назад")
                    }.padding()
                    Spacer()
                }

            }
            
            ZStack {
                if isRoot {
                    content
                        .animation(self.animation)
                        .transition(viewModel.navigationType == .push ? transition.push : transition.pop)
                    // .environmentObject(self.viewModel)
                } else {
                    viewModel.currentScreen?.screenView
                        .animation(self.animation)
                        .transition(viewModel.navigationType == .push ? transition.push : transition.pop)
                    //.environmentObject(viewModel)
                }
            }
            Spacer()
        }
    }
}

protocol IItemView: View {
    var listener: INavigationContainer? {get set}
}

protocol INavigationContainer {
    
    func push<Content: View & IItemView>(view: Content)
    func pop()
    func popToRoot()
}


struct NavigationItemContainer<Content>: View, INavigationContainer where Content: View & IItemView {
    var viewModel = NavigationViewModel.shared
    //@EnvironmentObject var vm: NavigationViewModel
    
    @State var title: String = ""
    
    private var contentView: Content
    
    public init(content: Content, _ title: String = "") {
        self.contentView = content
        self.contentView.listener = self
    }
    
    var body: some View {
        return ZStack {
            self.contentView
        }.background(Color.white)
    }
    
    
    func push<Content: View & IItemView>(view: Content) {
        let item = NavigationItemContainer<Content>(content: view)
        self.viewModel.push(AnyView(view))
    }
    
    func pop() {
        self.viewModel.pop()
    }
    
    func popToRoot() {
        self.viewModel.popToRoot()
    }
    
}

