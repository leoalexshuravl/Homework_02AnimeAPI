//
//  AnimeDetailXMLParser.swift
//  HomeworkAnimeAPI
//
//  Created by Журавлев Лев on 01.12.2023.
//

import Foundation

class AnimeDetailXMLParser: NSObject, XMLParserDelegate {
    var currentElement: String?
    var animeDetails: AnimeDetailsModel?
    var relatedPrev: Related?
    var infoList: [Info] = []
    var ratings: Ratings?
    var episodes: [Episode] = []
    var reviews: [Review] = []
    var releases: [Release] = []
    var newsList: [News] = []
    var staffList: [Staff] = []
    var castList: [Cast] = []
    var creditList: [Credit] = []
    var currentImageURL: String?

    func parseXML(xmlString: String) {     
        print(xmlString)
        if let data = xmlString.data(using: .utf8) {
            let parser = XMLParser(data: data)
            parser.delegate = self
            parser.parse()
        }
    }

    // MARK: - XMLParserDelegate methods

    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String: String] = [:]) {
        currentElement = elementName

        switch elementName {
        case "anime", "manga", "ONA", "TV" :
            animeDetails = AnimeDetailsModel(
                id: attributeDict["id"] ?? "",
                gid: attributeDict["gid"] ?? "",
                type: attributeDict["type"] ?? "",
                name: attributeDict["name"] ?? "",
                precision: attributeDict["precision"] ?? "",
                generatedOn: attributeDict["generated-on"] ?? "",
                relatedPrev: nil,
                infoList: [],
                ratings: nil,
                episodes: [],
                reviews: [],
                releases: [],
                newsList: [],
                staffList: [],
                castList: [],
                creditList: [], 
                currentImageURL: attributeDict["currentImageURL"] ?? ""
            )
        case "related-prev":
            relatedPrev = Related(rel: attributeDict["rel"] ?? "", id: attributeDict["id"] ?? "")
        case "info":
            infoList.append(Info(gid: attributeDict["gid"] ?? "", type: attributeDict["type"] ?? "", lang: attributeDict["lang"], value: ""))
        case "ratings":
            ratings = Ratings(
                nbVotes: Int(attributeDict["nb_votes"] ?? "") ?? 0,
                weightedScore: Double(attributeDict["weighted_score"] ?? "") ?? 0.0,
                bayesianScore: Double(attributeDict["bayesian_score"] ?? "") ?? 0.0
            )
        case "episode":
            episodes.append(Episode(num: Int(attributeDict["num"] ?? "") ?? 0, title: Title(gid: "", lang: "", value: "")))
        case "review":
            reviews.append(Review(href: attributeDict["href"] ?? "", value: ""))
        case "release":
            releases.append(Release(date: attributeDict["date"] ?? "", href: attributeDict["href"] ?? ""))
        case "news":
            newsList.append(News(datetime: attributeDict["datetime"] ?? "", href: attributeDict["href"] ?? "", value: ""))
        case "staff":
            staffList.append(Staff(task: "", person: Person(id: attributeDict["id"] ?? "", value: "")))
        case "cast":
            castList.append(Cast(gid: attributeDict["gid"] ?? "", lang: attributeDict["lang"] ?? "", role: "", person: Person(id: attributeDict["id"] ?? "", value: "")))
        case "credit":
            creditList.append(Credit(task: "", company: Company(id: attributeDict["id"] ?? "", value: "")))
        case "img":
            currentImageURL = attributeDict["src"]
        default:
            break
        }
    }

    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if let currentElement = currentElement {
            switch currentElement {
            case "info":
                if let currentInfo = infoList.last {
                    infoList[infoList.count - 1] = Info(gid: currentInfo.gid, type: currentInfo.type, lang: currentInfo.lang, value: string)
                }
            case "episode":
                if let currentEpisode = episodes.last {
                    episodes[episodes.count - 1] = Episode(num: currentEpisode.num, title: Title(gid: "", lang: "", value: string))
                }
            case "cast":
                if let currentCast = castList.last {
                    castList[castList.count - 1] = Cast(gid: currentCast.gid, lang: currentCast.lang, role: string, person: currentCast.person)
                }
            case "credit":
                if let currentCredit = creditList.last {
                    creditList[creditList.count - 1] = Credit(task: string, company: currentCredit.company)
                }
            default:
                break
            }
        }
    }

    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        currentElement = nil
    }

    func parserDidEndDocument(_ parser: XMLParser) {
        animeDetails?.relatedPrev = relatedPrev
        animeDetails?.infoList = infoList
        animeDetails?.ratings = ratings
        animeDetails?.episodes = episodes
        animeDetails?.reviews = reviews
        animeDetails?.releases = releases
        animeDetails?.newsList = newsList
        animeDetails?.staffList = staffList
        animeDetails?.castList = castList
        animeDetails?.creditList = creditList
        animeDetails?.currentImageURL = currentImageURL ?? ""

        // Use the 'animeDetails' instance as needed
        if let animeDetails = animeDetails {
            print(animeDetails)
        }
    }
}


class MangaParser: NSObject, XMLParserDelegate {
    var currentElement: String = ""
    var mangaID: String?
    var mangaGID: String?
    var mangaType: String?
    var mangaName: String?
    var mainTitle: String?
    var altTitleEN: String?
    var altTitleJA: String?
    var vintage: String?
    var officialWebsite: String?
    var imageURL: String?
    var staff: String?
    var staffName: String?
    
    func parseXML(xmlString: String) {
        print(xmlString)
        if let data = xmlString.data(using: .utf8) {
            let parser = XMLParser(data: data)
            parser.delegate = self
            parser.parse()
        }
    }
    
    // MARK: - XMLParserDelegate methods
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        currentElement = elementName
        
        if elementName == "manga" {
            mangaID = attributeDict["id"]
            mangaGID = attributeDict["gid"]
            mangaType = attributeDict["type"]
            mangaName = attributeDict["name"]
        }
        
        if elementName == "info" {
            if let type = attributeDict["type"], let lang = attributeDict["lang"] {
                switch type {
                case "Main title":
                    mainTitle = lang == "JA" ? attributeDict["src"] : nil
                case "Alternative title":
                    if lang == "EN" {
                        altTitleEN = attributeDict["src"]
                    } else if lang == "JA" {
                        altTitleJA = attributeDict["src"]
                    }
                case "Vintage":
                    vintage = attributeDict["src"]
                case "Official website":
                    officialWebsite = attributeDict["href"]
                default:
                    return
                }
            }
        }
        
        if elementName == "staff" {
            staff = attributeDict["gid"]
        }
        
        if elementName == "person" {
            staffName = attributeDict["id"]
        }
        
        if elementName == "img" {
            imageURL = attributeDict["src"]
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        // Implement if needed
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        // Implement if needed
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        // Printing parsed data for demonstration
        print("Manga ID: \(mangaID ?? "")")
        print("Manga GID: \(mangaGID ?? "")")
        print("Manga Type: \(mangaType ?? "")")
        print("Manga Name: \(mangaName ?? "")")
        print("Main Title: \(mainTitle ?? "")")
        print("Alternative Title (EN): \(altTitleEN ?? "")")
        print("Alternative Title (JA): \(altTitleJA ?? "")")
        print("Vintage: \(vintage ?? "")")
        print("Official Website: \(officialWebsite ?? "")")
        print("Image URL: \(imageURL ?? "")")
        print("Staff: \(staff ?? "")")
        print("Staff Name: \(staffName ?? "")")
    }
}

// Example usage
let xmlResponse = """
<ann><manga id="32562" gid="921303366" type="manga" name="Ano Ko wa Shōjo no Furi o Shite" precision="manga" generated-on="2024-03-24T07:07:44Z">
<related-prev rel="serialized in" id="31108"/>
<info gid="1866478073" type="Picture" src="https://cdn.animenewsnetwork.com/thumbnails/fit200x200/encyc/A32562-1866478073.1711252797.jpg" width="141" height="200"><img src="https://cdn.animenewsnetwork.com/thumbnails/fit200x200/encyc/A32562-1866478073.1711252797.jpg" width="141" height="200"/><img src="https://cdn.animenewsnetwork.com/thumbnails/max500x600/encyc/A32562-1866478073.1711252797.jpg" width="423" height="600"/><img src="https://cdn.animenewsnetwork.com/images/encyc/A32562-1866478073.1711252797.jpg" width="1058" height="1500"/></info>
<info gid="2495277966" type="Main title" lang="JA">Ano Ko wa Shōjo no Furi o Shite</info>
<info gid="2591424706" type="Alternative title" lang="EN">She Pretends to Be a Girl</info>
<info gid="1385612552" type="Alternative title" lang="JA">あの子は少女の振りをして</info>
<info gid="1122429651" type="Vintage">2023-06-22 (serialized on Sunday Webry)</info>
<info gid="1994835251" type="Official website" lang="JA" href="https://www.sunday-webry.com/episode/4856001361354847616">第1話 初冠 / あの子は少女の振りをして - 田中文 | サンデーうぇぶり</info>
<staff gid="3750705223"><task>Story &amp; Art</task><person id="235550">Aya Tanaka</person></staff></manga></ann>
"""

