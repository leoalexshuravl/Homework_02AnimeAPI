//
//  XMLParser.swift
//  HomeworkAnimeAPI
//
//  Created by Журавлев Лев on 26.11.2023.
//

import Foundation
import SwiftUI

class ReportXMLParserDelegate: NSObject, XMLParserDelegate {
    var report: Report?
    var currentItem: ReportItem?
    var currentElement: String?

    func parse(data: Data) -> Report? {
        let parser = XMLParser(data: data)
        parser.delegate = self
        parser.parse()
        return report
    }

    // MARK: - ReportXMLParserDelegate methods

    func parser(
        _ parser: XMLParser,
        didStartElement elementName: String,
        namespaceURI: String?,
        qualifiedName qName: String?,
        attributes attributeDict: [String: String] = [:]
    ) {
        currentElement = elementName

        if elementName == "report" {
            report = Report(
                skipped: attributeDict["skipped"] ?? "",
                listed: attributeDict["listed"] ?? "", 
                args: ReportArgs(type: "", name: "", search: ""),
                items: []
            )
        } else if elementName == "item" {
            currentItem = ReportItem(id: "", gid: "", type: "", name: "", precision: "", vintage: nil)
        }
    }

    func parser(_ parser: XMLParser, foundCharacters string: String) {
        switch currentElement {
        case "type":
            currentItem?.type += string
        case "name":
            currentItem?.name += string
        case "id":
            currentItem?.id += string
        case "gid":
            currentItem?.gid += string
        case "precision":
            currentItem?.precision += string
        case "vintage":
            currentItem?.vintage = string
        case "args":
            // Handle args element
            break
        default:
            break
        }
    }

    func parser(
        _ parser: XMLParser,
        didEndElement elementName: String,
        namespaceURI: String?,
        qualifiedName qName: String?
    ) {
        if elementName == "item" {
            if let currentItem = currentItem {
                report?.items.append(currentItem)
            }
            currentItem = nil
        }
        currentElement = nil
    }
    
    func getCachedReport(
        completion: ((Report?, Error?) -> Void)? = nil) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                let report = Report(
                    skipped: "0",
                    listed: "50",
                    args: HomeworkAnimeAPI.ReportArgs(type: "", name: "", search: ""), items: [HomeworkAnimeAPI.ReportItem(id: "30862", gid: "1679754853", type: "TV", name: "Red Cat Ramen", precision: "TV", vintage: nil), HomeworkAnimeAPI.ReportItem(id: "30852", gid: "2980330406", type: "TV", name: "Meitō \"Isekai no Yu\" Kaitaku-ki", precision: "TV", vintage: Optional("2024-01")), HomeworkAnimeAPI.ReportItem(id: "30847", gid: "2282880666", type: "ONA", name: "Darkness Heels", precision: "ONA", vintage: nil), HomeworkAnimeAPI.ReportItem(id: "30839", gid: "1796730238", type: "ONA", name: "Terapagos no Kirakira Tanken-ki", precision: "ONA", vintage: Optional("2023-12-01 to 2023-12-15")), HomeworkAnimeAPI.ReportItem(id: "30817", gid: "1716637934", type: "TV", name: "Sword of the Demon Hunter", precision: "TV", vintage: Optional("2024")), HomeworkAnimeAPI.ReportItem(id: "30789", gid: "2848076692", type: "TV", name: "Blue Box", precision: "TV", vintage: nil), HomeworkAnimeAPI.ReportItem(id: "30707", gid: "1133809363", type: "TV", name: "Magical Girl and The Evil Lieutenant Used to Be Archenemies", precision: "TV", vintage: nil), HomeworkAnimeAPI.ReportItem(id: "30697", gid: "1028088416", type: "ONA", name: "True Beauty", precision: "ONA", vintage: Optional("2024")), HomeworkAnimeAPI.ReportItem(id: "30685", gid: "2054605606", type: "TV", name: "Plus-Sized Misadventures in Love", precision: "TV", vintage: nil), HomeworkAnimeAPI.ReportItem(id: "30684", gid: "2650365482", type: "ONA", name: "Great Pretender razbliuto", precision: "ONA", vintage: Optional("2024")), HomeworkAnimeAPI.ReportItem(id: "30664", gid: "2133943012", type: "TV", name: "Yamishibai: Japanese Ghost Stories", precision: "TV 12/2024", vintage: Optional("2024-01")), HomeworkAnimeAPI.ReportItem(id: "30616", gid: "717194814", type: "ONA", name: "SAND LAND: THE SERIES", precision: "ONA", vintage: Optional("2024")), HomeworkAnimeAPI.ReportItem(id: "30603", gid: "2616334977", type: "special", name: "Reborn!: Here Comes a Vongola Family-Style School Trip!", precision: "special", vintage: Optional("2009-10-12")), HomeworkAnimeAPI.ReportItem(id: "30594", gid: "2555681323", type: "ONA", name: "Terminator", precision: "ONA", vintage: nil), HomeworkAnimeAPI.ReportItem(id: "30593", gid: "1854531257", type: "TV", name: "Modaete yo, Adam-kun", precision: "TV", vintage: nil), HomeworkAnimeAPI.ReportItem(id: "30591", gid: "1848258851", type: "TV", name: "After-School Hanako-kun", precision: "TV 2", vintage: Optional("2024-10")), HomeworkAnimeAPI.ReportItem(id: "30590", gid: "743776557", type: "TV", name: "Toilet-Bound Hanako-kun", precision: "TV 2", vintage: nil), HomeworkAnimeAPI.ReportItem(id: "30589", gid: "2256934160", type: "TV", name: "New Gate", precision: "TV", vintage: Optional("2024")), HomeworkAnimeAPI.ReportItem(id: "30587", gid: "2290224655", type: "TV", name: "Nanare Hananare", precision: "TV", vintage: Optional("2024")), HomeworkAnimeAPI.ReportItem(id: "30585", gid: "3146572362", type: "ONA", name: "World Maker", precision: "ONA", vintage: Optional("2024-01")), HomeworkAnimeAPI.ReportItem(id: "30584", gid: "1923677801", type: "OAV", name: "Frank", precision: "OAV by Naomi Nagata", vintage: Optional("2005-11-25")), HomeworkAnimeAPI.ReportItem(id: "30582", gid: "2888154977", type: "OAV", name: "Frank", precision: "OAV by COCOA", vintage: Optional("2003")), HomeworkAnimeAPI.ReportItem(id: "30581", gid: "2875180734", type: "OAV", name: "Frank and the Blessing of Reverse Containment", precision: "OAV", vintage: Optional("2004")), HomeworkAnimeAPI.ReportItem(id: "30579", gid: "2192924434", type: "OAV", name: "Hi-Rise Hopper", precision: "OAV", vintage: Optional("2005-11-25")), HomeworkAnimeAPI.ReportItem(id: "30578", gid: "3192296497", type: "OAV", name: "Visions of Frank", precision: "OAV", vintage: nil), HomeworkAnimeAPI.ReportItem(id: "30577", gid: "2740652939", type: "OAV", name: "Frank", precision: "OAV by Eri Yoshimura", vintage: Optional("2005-11-25")), HomeworkAnimeAPI.ReportItem(id: "30574", gid: "1549474057", type: "TV", name: "Is It Wrong to Try to Pick Up Girls in a Dungeon? V", precision: "TV", vintage: nil), HomeworkAnimeAPI.ReportItem(id: "30573", gid: "1634164347", type: "OAV", name: "Vision of Escaflowne: Best Collection", precision: "OAV", vintage: Optional("1996-10-25 to 1997-04-25")), HomeworkAnimeAPI.ReportItem(id: "30572", gid: "903295640", type: "movie", name: "IDOLiSH7 the Movie LIVE 4bit BEYOND THE PERiOD", precision: "movie", vintage: Optional("2023-05-20")), HomeworkAnimeAPI.ReportItem(id: "30566", gid: "2513617798", type: "TV", name: "I Parry Everything", precision: "TV", vintage: nil), HomeworkAnimeAPI.ReportItem(id: "30565", gid: "2804464043", type: "TV", name: "I Want to Escape from Princess Lessons", precision: "TV", vintage: nil), HomeworkAnimeAPI.ReportItem(id: "30562", gid: "2200669112", type: "movie", name: "Shi ga Utsukushii Nante Dare ga Itta", precision: "movie", vintage: Optional("2023-12-22")), HomeworkAnimeAPI.ReportItem(id: "30555", gid: "2675205702", type: "ONA", name: "Ple Ple Pleiades x Kage-jitsu!", precision: "ONA", vintage: Optional("2023-10-31")), HomeworkAnimeAPI.ReportItem(id: "30554", gid: "316765305", type: "ONA", name: "Kage-jitsu! Second", precision: "ONA", vintage: Optional("2023-10-05")), HomeworkAnimeAPI.ReportItem(id: "30553", gid: "1550920498", type: "ONA", name: "Kage-jitsu!", precision: "ONA", vintage: Optional("2022-10-26 to 2023-02-15")), HomeworkAnimeAPI.ReportItem(id: "30546", gid: "3291085872", type: "TV", name: "Vampire Dormitory", precision: "TV", vintage: Optional("2024")), HomeworkAnimeAPI.ReportItem(id: "30535", gid: "3397755728", type: "TV", name: "Campfire Cooking in Another World with My Absurd Skill", precision: "TV 2", vintage: nil), HomeworkAnimeAPI.ReportItem(id: "30514", gid: "2054189499", type: "movie", name: "Hina is Beautiful", precision: "movie", vintage: Optional("2023-05-21")), HomeworkAnimeAPI.ReportItem(id: "30498", gid: "2157857670", type: "TV", name: "Totonoe! Sakuma-kun by &sauna", precision: "TV", vintage: Optional("2023-11-12")), HomeworkAnimeAPI.ReportItem(id: "30497", gid: "1864736175", type: "TV", name: "Mukuwarenakatta Murabito A, Kizoku ni Hirowarete Dekiai Sareru Ue ni", precision: "TV", vintage: nil), HomeworkAnimeAPI.ReportItem(id: "30496", gid: "3643598359", type: "ONA", name: "Detective Pikachu & the Mystery of the Missing Flan", precision: "ONA", vintage: Optional("2023-10-25")), HomeworkAnimeAPI.ReportItem(id: "30491", gid: "2708489259", type: "TV", name: "Kumarba", precision: "TV", vintage: Optional("2024-04")), HomeworkAnimeAPI.ReportItem(id: "30476", gid: "2570726194", type: "TV", name: "Karasu wa Aruji o Erabanai", precision: "TV", vintage: Optional("2024-04")), HomeworkAnimeAPI.ReportItem(id: "30463", gid: "2195149663", type: "TV", name: "Sumikko Gurashi Sorairo no Mainichi", precision: "TV", vintage: Optional("2023-10-26")), HomeworkAnimeAPI.ReportItem(id: "30447", gid: "2519922203", type: "TV", name: "Ageo to Tim", precision: "TV", vintage: Optional("2024-04")), HomeworkAnimeAPI.ReportItem(id: "30445", gid: "2737475636", type: "ONA", name: "Maomao no Hitorigoto", precision: "ONA", vintage: Optional("2023-10-23")), HomeworkAnimeAPI.ReportItem(id: "30444", gid: "1919579751", type: "TV", name: "Yakuza Fiancé: Raise wa Tanin ga Ii", precision: "TV", vintage: nil), HomeworkAnimeAPI.ReportItem(id: "30427", gid: "1757380186", type: "OAV", name: "License to Sleaze", precision: "OAV", vintage: Optional("2012-11-16")), HomeworkAnimeAPI.ReportItem(id: "30425", gid: "2818809341", type: "OAV", name: "Maneaters Gang: Last Order", precision: "OAV", vintage: Optional("2013-06-21")), HomeworkAnimeAPI.ReportItem(id: "30423", gid: "2216923770", type: "OAV", name: "Confessions of a Slutty Wife", precision: "OAV", vintage: Optional("2013-07-19"))])
                completion?(report, nil)
            }
        }
}



