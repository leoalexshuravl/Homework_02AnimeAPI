//
//  HomeworkAnimeAPIApp.swift
//  HomeworkAnimeAPI
//
//  Created by Журавлев Лев on 26.11.2023.
//

import SwiftUI

@main
struct HomeworkAnimeAPIApp: App {
    var body: some Scene {
        WindowGroup {
//            AnimeListView()
//            PushNotificationView(content: NotificationCardView())
//            ContentView()
            NavigationContainerView {
                AnimeListView()
            }
        }
    }
}
