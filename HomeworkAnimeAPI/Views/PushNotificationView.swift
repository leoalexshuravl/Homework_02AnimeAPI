//
//  PushNotificationView.swift
//  HomeworkAnimeAPI
//
//  Created by Журавлев Лев on 01.02.2024.
//

import SwiftUI

struct PushNotificationView<Content: View>: View {
    @State private var isNotificationVisible = false
    let content: Content
    
    var body: some View {
        ZStack {
            VStack {
                Text("Main Content")
                    .padding()
                Spacer()
            }
            
            if isNotificationVisible {
                GeometryReader { geometry in
                    VStack {
                        Spacer()
                        content
                            .frame(width: geometry.size.width, height: 100)
                    }
                }
                .transition(.move(edge: .bottom))
                .animation(.easeInOut)
                .zIndex(1) // Bring notification to the front
            }
        }
        .edgesIgnoringSafeArea(.bottom)
        .onTapGesture {
            withAnimation {
                isNotificationVisible.toggle()
            }
        }
    }
}

struct NotificationCardView: View {
    var body: some View {
        RoundedRectangle(cornerRadius: 10)
            .foregroundColor(Color.blue)
            .overlay(
                Text("New Notification")
                    .foregroundColor(.white)
            )
    }
}

struct PushNotificationView_Previews: PreviewProvider {
    static var previews: some View {
        PushNotificationView(content: Text("Hello world"))
    }
}
