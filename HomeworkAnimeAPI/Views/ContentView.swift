//
//  SwiftUIView.swift
//  HomeworkAnimeAPI
//
//  Created by Журавлев Лев on 13.02.2024.
//

import SwiftUI

struct ContentView: View, IItemView {
    let data = ["Item 1", "Item 2", "Item 3", "Item 4", "Item 5"]
    
    var listener: INavigationContainer?
    
    @State private var viewModels: [ContentCellViewModel] = []
    @State private var tappedCellFrame: CGRect?
    @State private var isVisible: Bool = false

    var body: some View {
        VStack {
            Text("Hello world")
            ZStack {
                List(data.indices, id: \.self) { index in
                    GeometryReader { geometry in
                        ZStack {
                            Text(self.data[index])
                        .onTapGesture {
                                print("tapp")
                                listener?.push(
                                    view: ContentCellView(
                                        viewModel: ContentCellViewModel(
                                            text: "Hello world",
                                            geometry: geometry,
                                            isVisible: true
                                        )
                                    )
                                )
                            }
                        }
                    }
                }
                if isVisible {
                    ForEach(viewModels) { contentCellViewModel in
                        if contentCellViewModel.isVisible {
                            ContentCellView(viewModel: contentCellViewModel)
                                .position(CGPoint(x: 100.0, y: 100.0))
                                .background(.red)
                        }
                    }
                }
            }
        }
    }
}

struct ContentCellViewModel: Identifiable {
    let id = UUID()
    let text: String
    var geometry: GeometryProxy
    var isVisible: Bool
}

struct ContentCellView: View, IItemView {
    var listener: INavigationContainer?
    
    
    let viewModel: ContentCellViewModel
    
    var body: some View {
        Text("Hello world")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


