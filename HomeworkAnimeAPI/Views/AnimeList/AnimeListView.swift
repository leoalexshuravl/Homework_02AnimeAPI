//
//  AnimeListView.swift
//  HomeworkAnimeAPI
//
//  Created by Журавлев Лев on 26.11.2023.
//

import SwiftUI


struct AnimeListView: View, IItemView {
    var listener: INavigationContainer?
    
    private var pickerSelection: [TypesOfAnimeContent] = [.manga, .anime, .ONA, .TV]
    @State var pickerSelectionIndex: Int = 0 {
        didSet { fetchData(for: pickerSelection[pickerSelectionIndex]) }
    }
    @ObservedObject var animeViewModel = AnimeListViewModel()
    
    
    var body: some View {
        NavigationView {
            VStack {
                // MARK: - Picker
                Picker("Anime", selection: $pickerSelectionIndex) {
                    ForEach(0..<pickerSelection.count, id: \.self) { index in
                        Text(pickerSelection[index].rawValue)
                    }
                }
                .pickerStyle(.segmented)
                .padding()
                .onChange(of: pickerSelectionIndex) { _, newValue in
                    fetchData(for: pickerSelection[newValue])
                }
                Spacer()
                
                // MARK: - Tab
                TabView(selection: $pickerSelectionIndex) {
                    ForEach(0..<pickerSelection.count) { index in
                        if animeViewModel.isLoadingState {
                            AnimeLoadingView()
                                .tag(index)
                        } else {
                            let items = animeViewModel.model.first(where: {$0.type == getCurrentSelection()})?.reportItems
                            if let items = items {
                                switch items.isEmpty {
                                case false:
                                    // MARK: - List
                                    List(items) { item in
                                        let recomended = [items.randomElement()].compactMap{$0}
                                        AnimeListViewCellView(
                                            item: item,
                                            recomended: recomended
                                        ).onAppear {
//                                            print(item.name)
                                        }
                                    }
                                    .background(Color.gray.opacity(0.2))
                                    .cornerRadius(10)
                                default:
                                    Text("This list is empty")
                                }
                            } else {
                                AnimeLoadingView()
                            }
                        }
                    }.onAppear {
                        if let items = animeViewModel.model.first(where: {$0.type == getCurrentSelection()})?.reportItems {
                            
                        } else {
                            fetchData(for: getCurrentSelection())
                        }
                        
                    }
                }
                .tabViewStyle(PageTabViewStyle(indexDisplayMode: .automatic))
            }
        }
}


    private func getCurrentSelection() -> TypesOfAnimeContent {
        pickerSelection[pickerSelectionIndex]
    }
    
    private func fetchData(for animeType: TypesOfAnimeContent) {
        animeViewModel.fetchReport(for: animeType)
//        animeViewModel.fetchCachedReport()
    }
}

#Preview {
    AnimeListView()
}

struct AnimeListViewCellView: View {
    
    let item: ReportItem
    let recomended: [ReportItem]?
    
    var body: some View {
        HStack {
            NavigationLink {
                AnimeDetailsView(
                    model: AnimeDetailModel(
                        animeName: item.name,
                        animeType: TypesOfAnimeContent(rawValue: item.precision) ?? .anime,
                        animeId: item.id
                    ), recomended: recomended?.map { item in
                        AnimeDetailModel(
                            animeName: item.name,
                            animeType: TypesOfAnimeContent(rawValue: item.precision) ?? .anime,
                            animeId: item.id
                        )
                    } ?? nil
                )
            } label: {
                VStack(alignment: .leading)
                {
                    Text(item.name)
                    Text(item.type).font(.caption)
                }
            }
            Spacer()
        }
    }
}

#Preview {
    AnimeListViewCellView(
        item: ReportItem(
            id: "id",
            gid: "gid",
            type: "manga",
            name: "Manga",
            precision: "precision"
        ), recomended: [
            ReportItem(
                id: "id",
                gid: "gid",
                type: "manga",
                name: "Manga",
                precision: "precision")
        ]
    )
}
