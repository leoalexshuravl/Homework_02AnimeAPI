//
//  AnimeListViewModel.swift
//  HomeworkAnimeAPI
//
//  Created by Журавлев Лев on 01.12.2023.
//

import SwiftUI

class AnimeListViewModel: ObservableObject {
        
    private let animeReportService = AnimeReportService()
    
    @Published var model: [AnimeListModel] = []
    @Published var isLoadingState: Bool = true
            
    func fetchReport(for animeType: TypesOfAnimeContent, completion: ((Result<Report?, Error>) -> Void)? = nil) {
        if let model = model.first(where: {$0.type == animeType }) { return }
        isLoadingState = true
        animeReportService.fetchReport(for: animeType) { [weak self] (result: Result<Report?, Error>) in
            DispatchQueue.main.async {
                switch result {
                case .success(let report):
                    let animeListModel = AnimeListModel(
                        type: animeType,
                        reportItems: report?.items ?? []
                    )
                    self?.model.removeAll { $0.type == animeType }
                    self?.model.append(animeListModel)
                    completion?(.success(report))
                case .failure(let error):
                    completion?(.failure(error))
                }
                self?.isLoadingState = false
            }
        }
    }
    
    func fetchCachedReport(completion: ((Result<Report?, Error>) -> Void)? = nil) {
        isLoadingState = true
        animeReportService.fetchCachedReport { [weak self] (result: Result<Report?, Error>) in
            switch result {
            case .success(let report):
                let animeListModel = AnimeListModel(
                    type: .anime,
                    reportItems: report?.items ?? []
                )
                self?.model.removeAll { $0.type == .anime }
                self?.model.append(animeListModel)
                self?.isLoadingState = false
                completion?(.success(report))
            case .failure(let error):
                completion?(.failure(error))
            }
            self?.isLoadingState = false
        }
    }
    
    func selectUniqueRandomElements<T>(from array: [T]) -> [T]? {
        guard array.count >= 3 else {
            return nil
        }
        
        let shuffledArray = array.shuffled()
        let selectedElements = Array(shuffledArray.prefix(3))
        
        return selectedElements
    }
}
