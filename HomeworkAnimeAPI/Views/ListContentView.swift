//
//  ContentView.swift
//  HomeworkAnimeAPI
//
//  Created by Журавлев Лев on 28.11.2023.
//

import SwiftUI

struct ListContentView: View {
    @State private var selectedSegment = 0
    @State private var isCellSelected = false
    
    var body: some View {
        VStack {
            Picker("Segments", selection: $selectedSegment) {
                Text("Section 1").tag(0)
                Text("Section 2").tag(1)
                Text("Section 3").tag(2)
            }
            .pickerStyle(SegmentedPickerStyle())
            .padding()
            
            ZStack {
                List {
                    ForEach(0..<5) { item in
                        ZStack(alignment: .leading) {
                            Text("\(item)")
                            GeometryReader { geometry in
                                Text("Text above cell")
                                    .position(
                                        x: geometry.frame(in: .global).minX,
                                        y: geometry.frame(in: .local).midY
                                    )
                                    .padding(.horizontal)
                            }
                        }
                    }
                }
                Text("Add to favotite")
            }
        }
    }
}

struct CellRowView: View {
    @Binding var isSelected: Bool
    
    var body: some View {
        Text("Cell Row")
            .frame(height: 50)
            .background(Color.green)
    }
}

struct CellView: View {
    var isSelected: Bool
    
    var body: some View {
        Text("Selected Cell")
            .background(Color.blue)
            .cornerRadius(10)
            .position(CGPoint(x: 10.0, y: 10.0))
    }
}

struct ListContentView_Previews: PreviewProvider {
    static var previews: some View {
        ListContentView()
    }
}
