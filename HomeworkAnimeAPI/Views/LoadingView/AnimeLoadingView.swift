//
//  AnimeLoadingView.swift
//  HomeworkAnimeAPI
//
//  Created by Журавлев Лев on 01.12.2023.
//

import SwiftUI

struct AnimeLoadingView: View {
    var body: some View {
        VStack {
//            GifImageView("loadingGif").frame(
//                width: 100,
//                height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/,
//                alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/
//            )
            ProgressView()
        }
    }
}

struct AnimeLoadingView_Previews: PreviewProvider {
    static var previews: some View {
        AnimeLoadingView()
    }
}
