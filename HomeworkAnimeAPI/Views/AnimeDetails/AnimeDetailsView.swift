//
//  AnimeDetails.swift
//  HomeworkAnimeAPI
//
//  Created by Журавлев Лев on 03.12.2023.
//

import SwiftUI

struct AnimeDetailsView: View {
    
    let model: AnimeDetailModel
    let recomended: [AnimeDetailModel]?
    @ObservedObject var viewModel = AnimeDetailsViewModel()
    @State var errorState: String?
    
    var body: some View {
        ScrollView {
            if let error = errorState { Text(error) }
            if let animeDetails = viewModel.animeDetails {
                ForEach(animeDetails.infoList) { info in
                    switch info.type.lowercased() {
                    case "picture":
                        if let url = URL(string: animeDetails.currentImageURL) {
                            URLImage(url: url)
                        }
                    default:
                        HStack(spacing: 20) {
                            Text(info.type).font(.title3)
                            Spacer()
                            Text(info.value).frame(width: 200, alignment: .leading)
                        }
                    }
                }
                Spacer()
                if let recomended = recomended {
                    VStack(alignment: .leading)
                    {
                        Text("see also").font(.title2)
                        ForEach(recomended) { (recomendedItem: AnimeDetailModel) in
                            HStack {
                                NavigationLink {
                                    AnimeDetailsView(
                                        model: AnimeDetailModel(
                                            animeName: recomendedItem.animeName,
                                            animeType: model.animeType ?? .anime,
                                            animeId: recomendedItem.animeId
                                        ),
                                        recomended: nil
                                    )
                                } label: {
                                    HStack()
                                    {
                                        Text(recomendedItem.animeName)
                                        Spacer()
                                        Text(recomendedItem.animeType.rawValue).font(.caption)
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                VStack {
                    AnimeLoadingView()
                    Text("\(model.animeName), \(model.animeId)")
                }
                
            }
        }.onAppear {
            fetchAnimeDetails(for: model.animeId)
        }.navigationTitle(viewModel.animeDetails?.name ?? "").navigationBarTitleDisplayMode(.automatic)
        
    }
    
    func fetchAnimeDetails(for: String) {
        viewModel.fetchAnimeDetails(
            type: model.animeType,
            for: model.animeId
        ) { result in
            switch result {
                case .failure(let error):
                    self.errorState = error.localizedDescription
                case .success(_):
                    self.errorState = nil
                    break
            }
        }
    }
}


#Preview {
    AnimeDetailsView(
        model: AnimeDetailModel(
            animeName: "AnimeName",
            animeType: .anime,
            animeId: "4658"
        ), recomended: [
            AnimeDetailModel(
                animeName: "AnimeName",
                animeType: .anime,
                animeId: "4658"
            )
        ]
    )
}


struct URLImage: View {
    let url: URL?
    
    init(url: URL?) {
        self.url = url
    }
    
    var body: some View {
        Group {
            if let url = url {
                AsyncImage(url: url) { phase in
                    switch phase {
                    case .empty:
                        HStack {
                            Text("picture is Loading").font(.subheadline)
                            Spacer()
                            ProgressView()
                        }
                        
                    case .success(let image):
                        image
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                    case .failure:
                        Image(systemName: "photo")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                    @unknown default:
                        EmptyView()
                    }
                }
            } else {
                Image(systemName: "photo")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            }
        }
    }
}
