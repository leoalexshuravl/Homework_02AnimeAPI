//
//  AnimeDetailsViewModel.swift
//  HomeworkAnimeAPI
//
//  Created by Журавлев Лев on 03.12.2023.
//

import Foundation

class AnimeDetailsViewModel: ObservableObject {
    @Published var animeDetails: AnimeDetailsModel?
    
    private let animeDetailService = AnimeDetailService()
        
    func fetchAnimeDetails(
        type: TypesOfAnimeContent,
        for animeId: String,
        completion: ((Result<AnimeDetailsModel?, Error>) -> Void)? = nil
    ) {
        animeDetails = nil
        animeDetailService.fetchAnimeDetails(type: type, for: animeId) { [weak self] (result: Result<AnimeDetailsModel, Error>) in
            switch result {
            case .success(let animeDetails):
                DispatchQueue.main.async {
                    self?.animeDetails = animeDetails
                    completion?(.success(animeDetails))
                }                
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
}
