//
//  AnimeDetailModel.swift
//  HomeworkAnimeAPI
//
//  Created by Журавлев Лев on 25.03.2024.
//

import Foundation

struct AnimeDetailModel: Identifiable {
    var id: String { animeName }
    let animeName: String
    let animeType: TypesOfAnimeContent
    let animeId: String
}

